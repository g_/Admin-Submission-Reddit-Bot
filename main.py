import re
import praw
import schedule
import settings

#Create reddit instance
reddit = praw.Reddit(user_agent = settings.REDDIT_USER_AGENT,
						client_id = settings.REDDIT_CLIENT_ID,
						client_secret = settings.REDDIT_CLIENT_SECRET,
						username = settings.REDDIT_USERNAME,
						password = settings.REDDIT_PASSWORD)

def save_submission(file, data):
	#Overwrites the file with data and closes it
	file.seek(0)
	file.write(data)
	file.truncate()
	file.close()

def handle_deletion():
	#Handles the deletion of past submission, returns a file object to be written to
	try:
		file = open("past_submission.txt", "r+")
	except FileNotFoundError:
		#Create new file if it doesn't exist
		file = open("past_submission.txt", "w+")
	try:
		past_submission_id = re.findall(r'(\w+)', file.read().lower())[0]
	except IndexError:
		past_submission_id = None
	try:
		reddit.submission(past_submission_id).delete()
		print("Deleted {} ".format(past_submission_id))
	except:
		#Not sure what can go wrong where, so EXCEPT EVERYTHING!
		print("Deleting past submission {} failed".format(past_submission_id))
	return file

def make_post(data):
	file = handle_deletion()

	#Send the post, notify user and sticky it
	post = reddit.subreddit(settings.REDDIT_SUBREDDIT).submit(title=settings.REDDIT_SUBMISSION_TITLE, selftext=data)
	if settings.ADMIN_NOTIFY_USER:
		reddit.redditor(settings.ADMIN_NOTIFY_USER).message(settings.REDDIT_SUBMISSION_TITLE, post.selftext)
	post.mod.sticky()

	save_submission(file, post.id)
	print("Made post: ", post.title, ":", post.id)

if __name__ == "__main__":
	print("Setting up task scheduler...")

	#Task Scheduler, every week at 12
	submission_text = open(settings.DATA_LOCATION, "r+").read()
	schedule.every(settings.ADMIN_DAYS_TO_WAIT).days.at("12:00").do(make_post,submission_text)
	while True:
		schedule.run_pending()