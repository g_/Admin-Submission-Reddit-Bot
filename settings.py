#Bot account details
REDDIT_USERNAME = ""
REDDIT_PASSWORD = ""
REDDIT_CLIENT_ID = ""
REDDIT_CLIENT_SECRET = ""
REDDIT_USER_AGENT = ""

#Which subreddit to monitor
REDDIT_SUBREDDIT = ""

#Title of the submission
REDDIT_SUBMISSION_TITLE = ""

#The relative path of the submission text
DATA_LOCATION = "data.txt"

#Which user to notify that the submission has been made
ADMIN_NOTIFY_USER = ""

ADMIN_DAYS_TO_WAIT = 7
